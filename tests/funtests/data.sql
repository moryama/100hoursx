INSERT INTO users (username, hash)
VALUES
    ('test user', 'pbkdf2:sha256:50000$TCI4GzcX$0de171a4f4dac32e3364c7ddc7c14f3e2fa61f2d17574483f7ffbb431b4acb2f'),
    ('test active user', 'pbkdf2:sha256:150000$0TsJPOwT$9e870823be70275bbb4c1c2b2abfc8fcba266262eb02342126727948400e38ca');

INSERT INTO topics (topicName, userId)
VALUES
    ('test topic', '2');

INSERT INTO subtopics (topicId, subtopicName, goalHours, doneMinutes, userId)
VALUES
    ('1', 'test subtopic A', '33', '0', '2'),
    ('1', 'test subtopic B', '25', '0', '2'),
    ('1', 'test subtopic C', '42', '0', '2');

INSERT INTO resources (subtopicId, resourceText, userId)
VALUES
    ('1', 'test resource 1', '2')
