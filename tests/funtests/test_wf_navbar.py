import pytest
from selenium import webdriver
import time


@pytest.mark.skip(reason="Fails against evidence. Currently not a priority.")
@pytest.mark.usefixtures('browser_setup')
class TestNavBar():

    def login(self):
        self.browser.get('http://127.0.0.1:5000/login')
        username = self.browser.find_element_by_id('username')
        password = self.browser.find_element_by_id('password')
        signin = self.browser.find_element_by_id('signin-button')
        username.send_keys('test user')
        password.send_keys('test')
        signin.click()

    def test_menu_shows_only_auth_options_if_user_is_not_loggedin(self):
        
        self.browser.get('http://127.0.0.1:5000')
        nav_bar = self.browser.find_element_by_tag_name('nav')
        nav_links = self.browser.find_elements_by_class_name('nav-item')

        assert 'Register' in [link.text for link in nav_links]
        assert 'Sign in' in [link.text for link in nav_links]
        assert 'My log' not in [link.text for link in nav_links]

    # @pytest.mark.usefixtures('selenium_auth')
    def test_menu_shows_all_options_if_user_is_loggedin(self):
        
        self.login() # TO DELETE WHEN FIXTURE WORKS
        time.sleep(5)
        self.browser.get('http://127.0.0.1:5000')
        nav_bar = self.browser.find_element_by_tag_name('nav')
        nav_links = self.browser.find_elements_by_class_name('nav-item')
        time.sleep(5)
        assert 'Register' not in [link.text for link in nav_links]
        assert 'Sign in' not in [link.text for link in nav_links]
        assert 'My log' in [link.text for link in nav_links]