import os 
import tempfile

import pytest
from selenium import webdriver

from OHHours import create_app
from OHHours.models.db import get_db, init_db

with open(os.path.join(os.path.dirname(__file__), 'data.sql'), 'rb') as f:
    _data_sql = f.read().decode('utf8')

@pytest.fixture
def app():
    '''Generate an app object configured for testing.'''
    db_file, db_path = tempfile.mkstemp()

    app = create_app({
        'TESTING': True, 
        'DATABASE': db_path,
    }) 

    with app.app_context():
        init_db()
        get_db().executescript(_data_sql)

    yield app

    os.close(db_file)
    os.unlink(db_path)
    os.remove(app.config['DATABASE'])

@pytest.fixture
def client(app):
    '''Return a test client object.'''
    return app.test_client()

@pytest.fixture
def runner(app):
    '''Return a command line runner object.'''
    return app.test_cli_runner()

    
class AuthActions(object):
    def __init__(self, client):
        self._client = client

    def login(self, username='test user', password='test', active_user=False):

        if active_user == True:
            username = 'test active user'  

        return self._client.post(
            '/login',
            data={'username': username, 'password': password}
        )
        

    def logout(self):
        return self._client.get('/logout')


@pytest.fixture
def auth(client):
    '''Return an authentication object that has login and logout methods.'''
    return AuthActions(client)


@pytest.fixture(scope='session')
def browser_setup(request):
    
    browser = webdriver.Firefox()
    session = request.node
    for item in session.items:
        cls = item.getparent(pytest.Class)
        setattr(cls.obj, "browser", browser)
    yield 
    browser.close()


@pytest.mark.usefixtures('browser_setup')
class SeleniumAuthActions(object):
    def login(self):
        self.browser.get('http://127.0.0.1:5000/login')
        self.username = self.browser.find_element_by_id('username')
        self.password = self.browser.find_element_by_id('password')
        self.signin = self.browser.find_element_by_id('signin-button')
        self.username.send_keys('test user')
        self.password.send_keys('test')
        self.signin.click()


@pytest.fixture
def selenium_auth(browser_setup):
    return SeleniumAuthActions

        






