import pytest
from flask import session

from OHHours.helpers import round_hours


@pytest.mark.parametrize(
    ('route', ), 
    (('newplan', ),
    ('userlog', ),
    ('changeplan', ),
    ('resources', ),
    ('deleteplan', )
    )   
)
def test_loginrequired_decorator_does_its_job(auth, client, route):
    auth.login()

    with client:
        response = client.get(route)
        assert session['user_id'] == 1


# @pytest.mark.parametrize(
#     ('minutes', 'roundup'),
#     ((20, 0.5), 
#     (29, 0.5),
#     (31, 1),
#     (59, 1),
#     (61, 1), 
#     (89, 1.5), 
#     (90, 1.5), 
#     (91, 1.5),
#     (110, 2))
# )
# def test_roundhours_function_does_its_job(minutes, roundup):
#     output = round_hours(minutes)
#     assert output == roundup