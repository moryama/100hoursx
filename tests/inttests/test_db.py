import sqlite3

import pytest
from OHHours.models.db import get_db

def test_get_and_close_db(app):
    # Check the connection is returned
    with app.app_context():
        db = get_db()
        assert db is get_db()

    # Check the connection gets closed
    with pytest.raises(sqlite3.ProgrammingError) as e:
        db.execute('SELECT BUBBA')
    
    assert 'closed' in str(e.value)

def test_init_db_command(runner, monkeypatch):
    class Recorder(object):
        called = False
    
    def fake_init_db():
        Recorder.called = True
    
    monkeypatch.setattr('OHHours.models.db.init_db', fake_init_db)
    result = runner.invoke(args=['init-db'])
    assert 'Initialized' in result.output
    assert Recorder.called
