import pytest
from flask import g, session, url_for
from OHHours.models.db import get_db 

def test_register(app, client):

    assert client.get('/register').status_code == 200
    
    credentials = {'username': 'a', 'password': 'a', 'confirmation': 'a'}
    client.post('/register', data=credentials)
    assert client.get('/newplan').status_code == 200

    with app.app_context():
        assert get_db().execute(
            "select * from users where username = 'a'",
        ).fetchone() is not None


def test_register_redirects_to_userlog_if_accessed_by_loggedin_user(app, auth, client):
    auth.login()
    response = client.get('/register')
    
    assert response.status_code == 302
    with app.app_context():
        assert response.headers['Location'] == url_for('userlog.userlog')


@pytest.mark.parametrize(
    ('username', 'password', 'confirmation', 'message'), (
    ('', '', '', b'Please choose a username'), 
    ('a', '', '', b'Please choose a password'), 
    ('a', 'a', 'b', b'Your password and confirmation'),
    ('test user', 'test', 'test', b'This username is taken'),
))

def test_validate_input_for_register(client, username, password, confirmation, message):
    response = client.post(
        '/register', 
        data={'username': username, 'password': password, 'confirmation': confirmation}
    )
    assert message in response.data


def test_login(client, auth):
    
    assert client.get('/login').status_code == 200
    auth.login()
    assert client.get('/userlog').status_code == 200

    with client:
        client.get('/')
        assert session['user_id'] == 1
        assert session['username'] == 'test user'
        assert session['has_plan'] == False
        

@pytest.mark.parametrize(
    ('username', 'password', 'message'), (
    ('wrongun', 'test', b'Please provide a valid username'), 
    ('test user', 'wrongpw', b'Please provide a valid password'), 
    ('', 'test', b'Please provide a username'),
    ('test user', '', b'Please provide a password'),
))

def test_validate_input_for_login(auth, username, password, message):
    response = auth.login(username, password)
    assert message in response.data
