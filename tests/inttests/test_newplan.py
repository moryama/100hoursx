import pytest
from flask import url_for

from OHHours.models.db import get_db

def test_newplan_form_only_available_if_logged_in(app, auth, client):
    
    with app.app_context():
        response = client.get('/newplan')
        assert response.headers['Location'] == url_for('auth.login')

def test_newplan_form_is_not_available_if_user_already_has_plan(app, auth, client):
    auth.login(active_user=True)
    response = client.get('/newplan')
    assert b'you already have a plan' in response.data


def test_can_save_newplan_in_database(app, auth, client):

    auth.login()
    newplan_data = {
        'topicName': 'test topic name',
        'subtopic': ['test sub 1', 'test sub 2', 'test sub 3'],
        'subtopicHours': [33, 33, 33]
    }

    response = client.post('/newplan', data=newplan_data)
    
    with app.app_context():
        
        topic = get_db().execute(
            "select topicName from topics where userId = '1'",
        ).fetchone()

        subtopics = get_db().execute(
            "select subtopicName from subtopics where userId = '1'",
        ).fetchall()

        hours = get_db().execute(
            "select goalHours from subtopics where userId = '1'",
        ).fetchall()
        
        assert topic == ('test topic name',) # because fetchone() returned a tuple
        assert len(subtopics) == 3
        assert hours[0] == (33,) # because fetchall() returned a list of tuples
        
        # After new plan is created should redirect to userlog
        assert response.headers['Location'] == url_for('userlog.userlog')





