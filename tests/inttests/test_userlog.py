import pytest
from flask import g, session, url_for

from OHHours.models.db import get_db
from OHHours.models.retr import retrieve_subtopics, retrieve_topic
from OHHours.models.add import add_session
    
def test_can_retrieve_log_from_database(app):
    userlog_data = [(1, 'test topic'),
                     [(1, 'test subtopic A', 33, 0), 
                      (2, 'test subtopic B', 25, 0), 
                      (3, 'test subtopic C', 42, 0)]
                    ]

    with app.app_context():
        conn = get_db()
        retrieved_topic = retrieve_topic(conn, '2')
        retrieved_subtopics = retrieve_subtopics(conn, '1')
    
        assert retrieved_topic and retrieved_subtopics in userlog_data

def test_page_shows_userplan(auth, client):
    auth.login(active_user=True)
    response = client.get('/userlog')
    topic_title = b'<h1 class="topic-title" name="topic-id" value="1">test topic</h1>'
    subA_title = b'<td class="subtopic-name">test subtopic A</td>'
    progressbarA = b'<div class="progress-bar subtopic-todo" role="progressbar" style="width: 100%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="bottom" title="hours to do">33h</div>'
    
    assert topic_title in response.data
    assert subA_title in response.data
    assert progressbarA in response.data

def test_can_manually_add_session_to_database(app, auth, client):
    auth.login(active_user=True)
    session_data = {'session-subtopic': 1,
                    'session-minutes': 35}

    response = client.post('/userlog', data=session_data)

    with app.app_context():
        conn = get_db()
        last_added_session = get_db().execute(
            "select * from sessions where sessionId = '1'",
        ).fetchone()

        assert response.headers['Location'] == url_for('userlog.userlog')

    assert session_data['session-subtopic'] in last_added_session
    assert session_data['session-minutes'] in last_added_session
    





