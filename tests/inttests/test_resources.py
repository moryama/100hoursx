import pytest
from flask import url_for

from OHHours.models.db import get_db



def test_resources_page_loads(app, auth, client): 
    auth.login(active_user=True)

    with app.app_context():
        response = client.get('/add_resources/<topic>/<subtopic>/<subtopic_id>')
        
        assert response.status_code == 200


def test_can_retrieve_resources_from_database(app, auth, client):
    auth.login(active_user=True)
    existing_resource = b'<div class="collapse" id="test subtopic A resources">\n              test resource 1'
    empty_resource_msg = b'You can add useful resources here.'
    

    with app.app_context():
        response = client.get('userlog')
        conn = get_db()

        assert existing_resource in response.data
        assert empty_resource_msg in response.data


def test_can_save_resources_in_database(app, auth, client):
    auth.login(active_user=True)
    new_resource_data = {'resource-text': 'test new resource 2'}
    
    response = client.post('/add_resources/test topic/test subtopic B/2', 
                data=new_resource_data)

    with app.app_context():

        retrieved_new_resource = get_db().execute(
            "select resourceText from resources where subtopicId = '2'",
            ).fetchone()

        assert retrieved_new_resource == ('test new resource 2',)
        assert response.headers['Location'] == url_for('userlog.userlog')


def test_can_update_resources_in_database(app, auth, client):
    auth.login(active_user=True)
    updated_resource_data = {'resource-text': 'test update resource 1'}

    response = client.post('/add_resources/test topic/test subtopic A/1', 
                data=updated_resource_data)

    with app.app_context():
        
        retrieved_updated_resource = get_db().execute(
            "select resourceText from resources where subtopicId = '1'",
            ).fetchone()

        assert retrieved_updated_resource == ('test update resource 1',)
        assert response.headers['Location'] == url_for('userlog.userlog')




