import pytest

def test_index_page_loads(client):
    assert client.get('/').status_code == 200