import pytest
from flask import url_for

from OHHours.models.db import get_db
from OHHours.models.add import add_subtopic


def test_changeplan_page_loads(app, auth, client):
    auth.login(active_user=True)
    response = client.get('/changeplan')
    assert response.status_code == 200


def test_can_add_subtopic_to_the_plan(app, auth, client):
    auth.login(active_user=True)

    with app.app_context():
        
        subtopics_before_posting = get_db().execute(
                "select subtopicId from subtopics where userId= '2' "
                ).fetchall()

        new_plan_data_including_a_new_subtopic = {'subtopic-name': ['test subtopic A', 
                                                                    'test subtopic B',
                                                                    'test subtopic C',
                                                                    'test new subtopic D'],
                                                 'subtopic-hours': ['33', '25', '42', '15'], 
                                                 'subtopic-id': ['1', '2', '3', 'new0'],
                                                 'topic-id': '1'}
        
        response = client.post('/changeplan', data=new_plan_data_including_a_new_subtopic)
        
        subtopics_after_posting = get_db().execute(
                "select subtopicId from subtopics where userId= '2' "
                ).fetchall()

        newly_inserted_subtopic = get_db().execute(
                "select subtopicName, goalHours, doneMinutes from subtopics where subtopicId= '4' "
                ).fetchone()
            
        assert len(subtopics_after_posting) == len(subtopics_before_posting) + 1
        
        assert newly_inserted_subtopic[0] == 'test new subtopic D'
        assert newly_inserted_subtopic[1] == 15
        assert newly_inserted_subtopic[2] == 0

        assert response.headers['Location'] == url_for('userlog.userlog')

        # check the total amount of hours is updated

def test_can_change_name_and_amount_of_hours_of_existing_subtopic(app, auth, client):
    auth.login(active_user=True)

    with app.app_context():
        
        subtopic_before_posting = get_db().execute(
                "select subtopicName, goalHours from subtopics where subtopicId= '2' "
                ).fetchone()

        new_plan_data_including_a_changed_subtopic = {'subtopic-name': ['test subtopic A', 'test subtopic Z',],
                                                      'subtopic-hours': ['33', '67'], 
                                                      'subtopic-id': ['1', '2'],
                                                      'topic-id': '1'}

        response = client.post('/changeplan', data=new_plan_data_including_a_changed_subtopic)

        subtopic_after_posting = get_db().execute(
                "select subtopicName, goalHours from subtopics where subtopicId= '2' "
                ).fetchone()
        
        assert subtopic_before_posting != subtopic_after_posting
        assert subtopic_after_posting == ('test subtopic Z', 67)

        assert response.headers['Location'] == url_for('userlog.userlog')
        

def test_can_remove_subtopic(app, auth, client):
    auth.login(active_user=True)

    with app.app_context():

        new_plan_data_including_a_removed_subtopic = {'subtopic-name': ['test subtopic A', 'test subtopic B',],
                                                      'subtopic-hours': ['33', '25'], 
                                                      'subtopic-id': ['1', '2'],
                                                      'topic-id': '1',
                                                      'to-delete': '3'}

        response = client.post('/changeplan', data=new_plan_data_including_a_removed_subtopic)

        search_old_subtopic = get_db().execute(
                "select subtopicName from subtopics where subtopicId= '3' "
                ).fetchone()
        
        assert search_old_subtopic is None



