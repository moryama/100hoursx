from OHHours import create_app


def test_config_for_testing():
    assert not create_app().testing
    assert create_app({'TESTING': True}).testing

def test_index_response(client):
    response = client.get('/')
    assert response.status_code == 200


