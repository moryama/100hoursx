import pytest
from flask import session, url_for

from OHHours.models.db import get_db

def test_deleteplan_page_loads(app, auth, client):
    auth.login(active_user=True)

    with app.app_context():
        response = client.get('/deleteplan')
        assert response.status_code == 200


def test_can_remove_whole_plan_(app, auth, client):
    auth.login(active_user=True)
    response = client.post('/deleteplan')
    
    with app.app_context():
        topic_in_db = get_db().execute(
            "select topicId from topics where userId='2'",
        ).fetchone()
        subtopics_in_db = get_db().execute(
            "select subtopicId from subtopics where userId='2'"
        ).fetchall()
        resources_in_db = get_db().execute(
            "select resourceId from resources where userId='2'"
        ).fetchall()
        sessions_in_db = get_db().execute(
            "select sessionId from sessions where userId='2'"
        ).fetchall()
    
        assert topic_in_db is None
        assert subtopics_in_db == []
        assert resources_in_db == []
        assert sessions_in_db == []

        assert response.headers['Location'] == url_for('userlog.userlog')
        # We want to user to be redirected to an emptylog page
        assert b'Your log is empty' in client.get(url_for('userlog.userlog')).data
