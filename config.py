import os
app_dir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    DEBUG = False
    TESTING = False
    SECRET_KEY = 'dev'
    DATABASE = "100hours.db"
    SERVER_NAME = "127.0.0.1:5000" # this is specified in order to be able to use app.app_context in tests

class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True
    DATABASE = "100hours.db"

class TestingConfig(Config):
    TESTING = True
    DATABASE = "test.db"
