# 100 Hours

Practice any skill for 100 hours and see where it gets you. 🏋   

This web application supports **self-learners** in **planning** and **monitoring** their learning process. 

It started as a Python script I wrote to assist me in my studies as a self-taught programmer. I then developed the idea into this web application as my final project for the [CS50](https://www.edx.org/cs50) course. Later on, I used the application to start learning functional and integration testing.

Check out the [demo app](http://onehundredhours.herokuapp.com/) on Heroku.

## Features:

- a basic **registration / login** system
- an interactive **guided path** to create a learning plan
- a drop-down form to **quickly update** your progress
- clear visual to **track progress**
- an **interface** with range sliders, number and text input to edit your plan


## Tools

- **Backend:** Python + Flask

- **Database:** PostgreSQL (deployment), SQLite3 (testing and development)

- **Interface:** Javascript + jQuery

- **Style:** Bootstrap

- **Testing:** Pytest, Selenium Webdriver

- **Deployed on:** Heroku

## Development notes

### Project structure
The app is built on Flask using the **application factory** and **blueprints** pattern. 

All blueprints are found in the `core` module and are registered in `__init__.py`. 

Templates can be found in separate `templates` folders, one for each `core` module. On the other hand, as blueprints won't naturally find local `static` folders, all CSS and JavaScript files are stored in a global `static` folder.

The module called `models` contains all functions related to accessing the SQLite database. Database initialization and connection are found in `db.py`.

A set of integration and functional tests is being added over time.

## Usage

```
$ git clone https://gitlab.com/moryama/100hours.git
$ cd 100hours
$ pip install requirements.txt
```
Initialize the database:

```
$ flask init-db
```

Run the server:
```
$ export FLASK_APP="OHHours"
$ flask run
```
Check your local host at `http://127.0.0.1:5000/`.


## License
No license is available at the moment. 
