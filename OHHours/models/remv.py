'''This module includes database delete functions used within the project.
'''

from .db import query_db

def delete_plan(conn, user_id):

    sql_sessions = '''DELETE FROM sessions
                         WHERE userId=?'''

    sql_resources = '''DELETE FROM resources
                          WHERE userId=?'''
    
    sql_subtopics = '''DELETE FROM subtopics
                          WHERE userId=?'''

    sql_topic = '''DELETE FROM topics
                      WHERE userId=?'''

    del_sessions = query_db(conn, user_id, sql_sessions, out_data='all')
    del_resources = query_db(conn, user_id, sql_resources, out_data='all')
    del_subtopics = query_db(conn, user_id, sql_subtopics, out_data='all')
    del_topic = query_db(conn, user_id, sql_topic, out_data='all')

    return True


def delete_subtopic(conn, subtopic_id):

    sql = '''DELETE FROM subtopics
             WHERE subtopicId=?'''
    
    del_subtopics  = query_db(conn, subtopic_id, sql, out_data='one')
    
    return True