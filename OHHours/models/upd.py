'''This module includes database update functions used within the project.
NOTE: Authentication functions are found in models/auth.py
'''

from .db import query_db


def update_minutes(conn, session_data):

    sql = '''UPDATE subtopics
             SET doneMinutes=?
             WHERE subtopicId=?'''
    total_minutes_updated = query_db(conn, session_data, sql, out_data='one')

    if not total_minutes_updated:
        return False
    
    return True


def update_resources(conn, resources_data):

    sql = '''UPDATE resources
             SET resourceText=?
             WHERE subtopicId=?'''
    plan_updated = query_db(conn, resources_data, sql, out_data='one')    

    if not plan_updated:
        return False
    
    return True
 

def update_subtopic(conn, subtopic_data):

    sql = '''UPDATE subtopics
              SET subtopicName=?, goalHours=?
              WHERE subtopicId=?'''
    subtopic_updated = query_db(conn, subtopic_data, sql, out_data='all')

    if not subtopic_updated:
        return False
    
    return True
    
