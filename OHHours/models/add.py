'''This module includes database insertion functions used within the project.
NOTE: Authentication functions are found in models/auth.py
'''

from .db import query_db


def add_resources(conn, resources_data):

    sql = '''INSERT INTO resources (subtopicId, resourceText, userId)
             VALUES (?,?,?)''' 
    resources_saved = query_db(conn, resources_data, sql, out_data='rowid')

    if not resources_saved:
        return False

    return True

def add_session(conn, session_data):

    sql = '''INSERT INTO sessions (subtopicId, datetime, minutes, userId)
             VALUES (?,?,?,?)'''
    session_saved = query_db(conn, session_data, sql, out_data='rowid')

    if not session_saved:
        return False
    
    return True

def add_subtopic(conn, subtopic_data):

    sql = '''INSERT INTO subtopics (topicId, subtopicName, goalHours, userId)
             VALUES (?,?,?,?)'''
    subtopic_saved = query_db(conn, subtopic_data, sql, out_data='all')

    if not subtopic_saved:
        return False

    return True

def add_topic(conn, topic_data):

    sql = '''INSERT INTO topics (topicName, userId)
             VALUES (?,?)'''
    topic_id = query_db(conn, topic_data, sql, out_data='rowid')
    return topic_id

