import sqlite3

import click
from flask import current_app, g
from flask.cli import with_appcontext

# Create a connection 
def get_db():
    '''Create a database connection

    :return: Connection object or None
    '''

    if 'db' not in g:
        g.db = sqlite3.connect(
            current_app.config['DATABASE'], 
            detect_types=sqlite3.PARSE_DECLTYPES
            )
    
    return g.db
    

# Close a connection
def close_db(e=None):
    db = g.pop('db', None)

    if db is not None:
        db.close()


# Query database and return data
def query_db(conn, inp_data, sql, out_data=None):

    db = conn.cursor()
    db.execute(sql, inp_data)

    if out_data == 'rowid':
        data = db.lastrowid
    elif out_data == 'one':
        data = db.fetchone()
    elif out_data == 'all':
        data = db.fetchall()

    if 'INSERT' or 'UPDATE' or 'DELETE' in sql:
        conn.commit()
    
    return data


# Run the SQL commands
def init_db():
    db = get_db()

    with current_app.open_resource('schema.sql') as f:
        db.executescript(f.read().decode('utf8'))

@click.command('init-db')
@with_appcontext
def init_db_command():
    '''Clear the existing data and create new tables.'''
    init_db()
    click.echo('Initialized database')

# Register these function which are then imported into the factory function __init__.py 
def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)






