'''This module includes database functions used within the project for authentication actions.'''

from .db import query_db


def register_user(conn, user_data):

    sql = '''INSERT INTO users (username, hash) VALUES (?,?)''' 
    user_id = query_db(conn, user_data, sql, out_data='rowid')
    return user_id


def retrieve_user(conn, username):
    '''return: [(user_id, username, hash)]'''

    sql = '''SELECT * FROM users WHERE username=?'''
    return query_db(conn, username, sql, out_data='one')


def user_has_plan(conn, user_id):

    sql = '''SELECT topicName FROM topics WHERE userId=?'''
    plan_exists = query_db(conn, user_id, sql, out_data='one')

    if plan_exists:
        return True

    return False


def username_available(conn, username):

    sql = '''SELECT username FROM users WHERE username=?'''
    username_exists = query_db(conn, username, sql, out_data='one')

    if username_exists:
        return False

    return True