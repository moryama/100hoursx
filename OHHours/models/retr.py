'''This module includes database retrieve functions used within the project.
NOTE: Authentication functions are found in models/auth.py
'''

from .db import query_db

def retrieve_last_session_datetime(conn, user_id):

   sql = '''SELECT datetime
            FROM sessions
            WHERE userId=?
            ORDER BY sessionId DESC'''
   last_session_datetime = query_db(conn, user_id, sql, out_data='one')
   
   return last_session_datetime



def retrieve_topic(conn, user_id):

   sql = '''SELECT topicId, topicName
            FROM topics
            WHERE userId=?'''
   topic_data = query_db(conn, user_id, sql, out_data='one')
   return topic_data

def retrieve_subtopics(conn, topic_id):

   sql = '''SELECT subtopicId, subtopicName, goalHours, doneMinutes
            FROM subtopics
            WHERE topicId=?''' 
   subtopics_data = query_db(conn, topic_id, sql, out_data='all')
   return subtopics_data


def retrieve_resources(conn, subtopic_id):

    sql = '''SELECT resourceText
             FROM resources
             WHERE subtopicId=?'''
    resources_data = query_db(conn, subtopic_id, sql, out_data='all')
    return resources_data


def user_has_plan(conn, user_id):

   sql = '''SELECT topicName 
            FROM topics
            WHERE userId=?'''
   existing_plan = query_db(conn, user_id, sql, out_data='one')

   if not existing_plan:
      return False
   
   return True