'''This module contains functions that can be used around the project
or exported to other projects.'''


from functools import wraps
from flask import redirect, session

def login_required(f):
    '''Decorate routes to require login.'''
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'user_id' not in session:
            return redirect('/login')
        return f(*args, **kwargs)
    return decorated_function


def round_hours(minutes):
    '''Convert minutes into hours then rounds them up.'''

    hm = divmod(minutes, 60)
    if hm[1] > 29:
        return hm[0] + 1
    else:
        return hm[0]
