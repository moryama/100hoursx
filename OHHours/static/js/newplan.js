/***************************************************************************
This module provides JavaScript functionalities that are used
to create a new plan within newplan.html:

- display wizard form
- validate form
- dinamically add input fields of type text, slider, number
- make slider and number fields interact with each other

This file is dependent on styles.css
***************************************************************************/


let currentTab = 0; // Set to be first tab (0)
showTab(currentTab); // Display current tab
$('#addSubtopic').click(onAddSubtopicField); // Activate custom button when needed


// Take care of "step circles"
function fixStepIndicator(n) {
    // remove the "active" class of all steps
    let i, x = document.getElementsByClassName("step");
    for (i = 0; i < x.length; i++) {
        x[i].className = x[i].className.replace(" active", "");
    }
    // Add "active" class to current step
    x[n].className += " active";
}

// Add an extra input field
let inputCount = 3; // default input fields
function onAddSubtopicField() {

    // Display guide message
    if (inputCount == 5) {
        alert("You can add as many as you want. Just take a moment to think: is it possible to keep it simple?");
    }

    inputCount = inputCount + 1;
    // Create new input element
    let subtopicInput = $('<input type="text" class="form-control mt-1 mb-1 mr-1" name="subtopic" required autocomplete="off" size="30" id="subtopic' + inputCount + '" />');

    // Create remove button
    let removeSubtopic = $('<button class="btn btn-outline-secondary button-light remove-button" type="button" id="removeSubtopic' + inputCount + '">Remove</button><br>');

    $(removeSubtopic).click(function onRemoveInputField() {
        $(this).parent().remove();
        inputCount -= 1;
    });

    // Create div
    let div = $('<div />');

    // Add new element and button to div
    div.append(subtopicInput);
    div.append(removeSubtopic);

    // Add div to the form
    $('.more-subtopics').append(div);

}

// Select which tab to show
function nextPrev(n) {
    // Close any open collapse
    $('.examples').attr('class', 'collapse examples');
    // Select the right tab to display
    let x = document.getElementsByClassName("tab");
    // Exit if any field in current tab is invalid
    if (event.target.id == "nextBtn" && !validateForm()) {
        return false;
    }
    // Hide current tab
    x[currentTab].style.display = "none";
    // Increase or decrease current tab by 1
    currentTab = currentTab + n;

    // If this is last tab, submit form
    if (currentTab >= x.length) {
        document.getElementById('newplanForm').submit();
        return false;
    }
    // Otherwise, display the correct tab
    showTab(currentTab);

}

// Show slider and box fields
function showProportionsForm() {

    // Get topic name
    let topicName = document.getElementById("topicName").value;
    // Get list of subtopics names
    let subtopics = [];
    let subtopicInputs = document.getElementsByName("subtopic");
    for(let i = 0; i < subtopicInputs.length; i++) {
        subtopics.push(subtopicInputs[i].value);
    }
    let totalSubtopics = subtopics.length;

    // Generate a proportion form
    function generateProportionForm(totalSubtopics, subtopicName, k) {
        // Generate div
        let div = $('<div></div>');
        div.attr('id', 'subtopic' + k)


        let defaultProportion = Math.round(100 / totalSubtopics);
        // Generate slider
        let slider = $('<br><input type="range" min="1" max="100">');
        slider.attr('id', 'slider' + k);
        slider.val(defaultProportion);
        slider.on('input', updateValue);
        // Generate box
        let box = $('<input type="number" min="1" max="100">');
        box.addClass('form-control mt-1 mb-1 number-box');
        box.attr('id', 'box' + k);
        box.attr('name', 'subtopicHours');
        box.val(defaultProportion);
        box.on('input', updateValue);
        // Populate div
        div.append(subtopicName);
        div.append(slider);
        div.append(box);
        div.append('<span>hours</span>');
        $('#proportions-fields').append(div);

    }
    // Generate the total hours box
    function generateTotalBox() {
        let totalBox = $('<input type="text" disabled>');
        totalBox.addClass('form-control mt-1 mb-1 number-box');
        totalBox.attr('id', 'totalHoursBox');
        totalBox.val('100');
        $('#proportions-fields').append(totalBox);
    }

    // Display the complete form:
    // Make sure form is empty
    $('#proportions-fields').empty()
    // Collapse button
    $('#proportions-fields').append('<small class="form-text text-muted"><a id="topicExample" data-toggle="collapse" href="#proportionExamples" aria-expanded="false" aria-controls="collapseExample">Show tips</a></small>');
    // Topic name
    $('#proportions-fields').append('<br><h3 class="topic-line">' + topicName +'</h3>');
    // Proportion fields for each subtopic
    for(let k = 0; k < totalSubtopics; k++) {
        generateProportionForm(totalSubtopics, subtopics[k], k);
    }

    // // Final line with total amount of hours
    $('#proportions-fields').append('<h4>Total hours</h4>');
    generateTotalBox();

}

// Make a tab visible
function showTab(n) {
    // Display specified tab of the form
    let x = document.getElementsByClassName("tab");
    x[n].style.display = "block";
    // Fix the Previous/Next buttons
    if (n == 0 || n == 1) {
        document.getElementById("prevBtn").style.display = "none";
    }
    else {
        document.getElementById("prevBtn").style.display = "inline";
    }
    if (n == 0) {
        document.getElementById("nextBtn").innerHTML = "Begin";
    }
    else if (n == (x.length - 1)) {
        document.getElementById("nextBtn").innerHTML = "Save";
    }
    else {
        document.getElementById("nextBtn").innerHTML = "Next";
    }
    // If second tab, show topic title
    if (n == 2) {
        showTopicName();
    }
    // If third tab, show custom form
    if (n == 3) {
        showProportionsForm();
    }
    // Display the correct step indicator
    fixStepIndicator(n)
 }

// Show the topic title
function showTopicName() {

    let topicName = document.getElementById("topicName").value;
    $('.topic-line').html(topicName);
}

// Make slider and box interact
function updateValue() {

    // Check which element triggered the event
    if (event.target.type == 'range') {
        // Update box value
        $(this).next().val(this.value);
    }
    else {
        // Update slider value
        $(this).prev().val(this.value);
    }

    // Update total hours
    let subtopicHours = document.getElementsByName('subtopicHours');
    let totalHours = 0;
    for (let i = 0; i < subtopicHours.length; i++) {
        if (parseInt(subtopicHours[i].value)) {
            totalHours += parseInt(subtopicHours[i].value);
        }
    document.getElementById('totalHoursBox').value = totalHours;
    }

}

// Make sure current tab fields are filled in
function validateForm(n) {
    // Validate form fields
    let x, y, i, valid = true;
    x = document.getElementsByClassName("tab");
    y = x[currentTab].getElementsByTagName("input");
    // Check every input field in current tab
    for (i = 0; i < y.length; i++) {
        // If a field is empty
        if (y[i].value == "") {
            // Mark as invalid
            y[i].className += " invalid";
            valid = false;
        }
    }

    // TODO THIS DOESN'T WORK
    if (n == 2 && $('#totalHoursBox').value != '100') {
        $('#totalHoursBox').className += " invalid";
    }

    // If valid status is true, mark step as finished and valid
    if (valid) {
        document.getElementsByClassName("step")[currentTab].className += " finish";
    }
    return valid;
}
