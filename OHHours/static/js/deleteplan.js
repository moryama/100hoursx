$(function () {
    // Remove whole plan permanently
    $('.delete-plan').click(function onConfirmDeletePlan() {
        // Ask the user to confirm the deletion of plan
        if (!confirm("This is permanent. Are you sure?")) {
            return false;
        }
        else {
            this.form.submit();
        }
    });
});
