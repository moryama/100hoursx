/***************************************************************************
This module provides JavaScript functionalities that are used
to change a current plan within changeplan.html:

- dinamically add input fields of kind text, slider, number
- make slider and number fields interact

***************************************************************************/


$('#changeplanForm').ready(function() {


    // Make slider and number boxes interact
    // For existing elements
    $('.proportion-input').on('input', updateValue);
    // For newly added elements
    $('#newSubtopicForms').on('input', ".proportion-input", updateValue);

    // Add a new elements
    let newSubtopicId = 0;
    $('#addSubtopic').click(function onAddSubtopicForm() {
        /* Add a complete subtopic form */

        // Add an extra subtopic form
        let subtopicForm = $("<div class=\"row subtopic-form new-form\" name=\"subtopicForm\" id=\"new" + newSubtopicId + "\"> \
                          <div class=\"col-sm-4\"> \
                          <input class=\"form-control mt-1 mb-1\" type=\"text\" name=\"subtopic-name\" required autocomplete=\"off\" size=\"30\"></div> \
                          <div class=\"col-sm-6\"> \
                          <input class=\"proportion-input\" type=\"range\" min=\"1\" max=\"100\" oninput=\"updateValue()\" value=\"1\"> \
                          <input class=\"proportion-input form-control mt-1 mb-1 number-box\" id=\"sub" + newSubtopicId + "hours\"  type=\"number\" name=\"subtopic-hours\" min=\"1\" max=\"100\" oninput=\"updateValue()\" required value=\"1\">hours</div> \
                          <div class=\"col-sm-1\"> \
                          <button class=\"btn btn-outline-secondary remove-new-subtopic\" type=\"button\" value=\"new" + newSubtopicId + "\">Remove</button></div> \
                          <input class=\"col-sm-1\" name=\"subtopic-id\" style=\"display:none\" value=\"new" + newSubtopicId + "\"></div>");

        // append subtopic form to main form
        $('#newSubtopicForms').append(subtopicForm);
        newSubtopicId ++;
    });

    // Remove a subtopic field
    // For existing elements
    $('.remove-subtopic').click(onRemoveSubtopic);
    // For newly added elements
    $("#newSubtopicForms").on("click", ".remove-new-subtopic", onRemoveSubtopic);

    // Save changes permanently
    $('.save-changes').click(function onSaveChanges() {
        // Ask the user to confirm before saving changes and send form

        if (!confirm("Are you sure you want to save the changes?")) {
            return false;
        }
        else {
            this.form.submit();
        }

    });


    function updateValue() {

        // Check which element triggered the event
        if (event.target.type == 'range') {
            // Update box value
            $(this).next().val(this.value);
            console.log("RANGE", this);
        }
        else {
            // Update slider value
            $(this).prev().val(this.value);
            console.log("BOX", this);
        }

        // Update total hours
        let subtopicHours = document.getElementsByName('subtopic-hours');
        let totalHours = 0;
        // Get the sum of values of all elements named 'subtopicHours'
        for (let i = 0; i < subtopicHours.length; i++) {
            if (parseInt(subtopicHours[i].value)) {
                totalHours += parseInt(subtopicHours[i].value);
            }
        document.getElementById('totalHoursBox').value = totalHours;
        }
    }

    function onRemoveSubtopic() {
        /* Removes a subtopic-form from the page (text field, slider, number box, remove button)
        The subtopic is removed from database only if/when the form is saved via 'Save' button

        Works on the premise that:
        button value == subtopicId
        subtopic-div id == subtopicId
        */

            buttonVal = $(this).val();

            // Search div of name 'subtopicForm' on the page
            let divs = document.getElementsByName("subtopicForm")

            for (let i = 0; i < divs.length; i++) {
                // Get div's id
                let divId = $(divs[i]).attr("id");

                // If the ids match
                if (divId == buttonVal) {
                    // Select element
                    let toRemoveDiv = document.getElementById(divId);
                    // Make sure only existing subtopics are sent for deletion
                    if (!divId.startsWith("new")) {
                        // Add to be sent by post for deletion
                        let subtopicDelete = $('<input type="text" name="to-delete" value="' + divId + '">');
                        $('.to-be-deleted').append(subtopicDelete);
                    }
                    // Remove it from page
                    $(toRemoveDiv).remove();

                }
            }

            return true;
        }


});
