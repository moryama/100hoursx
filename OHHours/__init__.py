import os 
from flask import Flask

def create_app(test_config=None):
    # Create a flask app
    app = Flask(__name__)
  
    if test_config is None:
        app.config.from_object('config.Config')
    else:
        app.config.from_object('config.TestingConfig')

    # Initialize database
    from .models import db
    db.init_app(app)
    
    # Register blueprints
    from .core.auth import auth
    app.register_blueprint(auth.bp)

    from .core.index import index
    app.register_blueprint(index.bp)

    from .core.plan import newplan
    app.register_blueprint(newplan.bp)

    from .core.user import userlog
    app.register_blueprint(userlog.bp)

    from .core.plan import resources
    app.register_blueprint(resources.bp)

    from .core.plan import changeplan
    app.register_blueprint(changeplan.bp)

    from .core.plan import deleteplan
    app.register_blueprint(deleteplan.bp)

    return app