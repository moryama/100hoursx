-- Text encoding used: UTF-8

-- Table: users
DROP TABLE IF EXISTS users;
CREATE TABLE users (
    userId INTEGER PRIMARY KEY UNIQUE NOT NULL, 
    username VARCHAR (255) UNIQUE NOT NULL, 
    hash TEXT UNIQUE NOT NULL
);

-- Table: topics
DROP TABLE IF EXISTS topics;
CREATE TABLE topics (
    topicId INTEGER PRIMARY KEY UNIQUE NOT NULL, 
    topicName VARCHAR (100) NOT NULL, 
    userId INTEGER REFERENCES users (userId) NOT NULL
);

-- Table: subtopics
DROP TABLE IF EXISTS subtopics;
CREATE TABLE subtopics (
    subtopicId INTEGER PRIMARY KEY UNIQUE NOT NULL, 
    topicId INTEGER REFERENCES topics (topicId) NOT NULL, 
    subtopicName VARCHAR (100) NOT NULL, 
    goalHours INTEGER NOT NULL, 
    doneMinutes INTEGER DEFAULT (0), 
    userId INTEGER REFERENCES users (userId) NOT NULL
);

-- Table: sessions
DROP TABLE IF EXISTS sessions;
CREATE TABLE sessions (
    sessionId INTEGER PRIMARY KEY UNIQUE NOT NULL, 
    subtopicId INTEGER REFERENCES subtopics (subtopicId) NOT NULL, 
    minutes INTEGER NOT NULL, datetime DATETIME NOT NULL, 
    userId INTEGER REFERENCES users (userId) NOT NULL
);

-- Table: resources
DROP TABLE IF EXISTS resources;
CREATE TABLE resources (
    resourceId INTEGER PRIMARY KEY UNIQUE NOT NULL, 
    subtopicId INTEGER REFERENCES subtopics (subtopicId) NOT NULL UNIQUE, 
    resourceText TEXT, 
    userId INTEGER REFERENCES users (userId) NOT NULL
);