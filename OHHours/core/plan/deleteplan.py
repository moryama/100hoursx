import functools
from flask import Blueprint, redirect, render_template, request, session, url_for
from OHHours.models.db import get_db
from OHHours.models.remv import delete_plan
from OHHours.helpers import login_required 


bp = Blueprint('deleteplan', __name__,
               template_folder='templates', 
               static_folder='static')

@bp.route('/deleteplan', methods=['GET', 'POST'])
@login_required
def delete_user_plan():

    user_id = session['user_id']

    if request.method == 'POST':

        conn = get_db()
        with conn:
            delete_plan(conn, (user_id, ))
        # Let's make sure any functionalities related to the existence of a plan become unavailable
        session['has_plan'] = False
        
        return redirect(url_for('userlog.userlog'))
    
    else:

        return render_template('deleteplan.html')

