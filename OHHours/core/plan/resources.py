import functools
from flask import Blueprint, redirect, render_template, request, session, url_for
from OHHours.helpers import login_required 
from OHHours.models.db import get_db
from OHHours.models.retr import retrieve_resources
from OHHours.models.add import add_resources
from OHHours.models.upd import update_resources


bp = Blueprint('resources', __name__, 
               template_folder='templates')

@bp.route('/add_resources/<topic>/<subtopic>/<subtopic_id>', methods=['GET', 'POST'])
@login_required
def add_new_resources(topic, subtopic, subtopic_id):
    user_id = session['user_id']

    if request.method == 'POST':

        # Save resources in database
        resource_text = request.form['resource-text']
        conn = get_db()
        with conn:

            try:
                row_id = add_resources(conn, (subtopic_id, resource_text, user_id))
            except:
                update_resources(conn, (resource_text, subtopic_id))
        return redirect(url_for('userlog.userlog'))

    else:

        # Retrieve resources from database
        conn = get_db()
        with conn:
            resource_text = retrieve_resources(conn, (subtopic_id, ))
            resources = []
            try:
                resources.append(resource_text[0])
            except:
                resources.append('You can add useful resources here.')

        return render_template('addresources.html', topic=topic, subtopic=subtopic, subtopic_id=subtopic_id, resources=resources[0])
