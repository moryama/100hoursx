import functools 
from flask import Blueprint, render_template, redirect, request, session, url_for
from OHHours.helpers import login_required 
from OHHours.models.db import get_db
from OHHours.models.add import add_topic, add_subtopic
from OHHours.models.retr import user_has_plan


bp = Blueprint('newplan', __name__, 
               template_folder='templates')


@bp.route('/newplan', methods=['GET', 'POST'])
@login_required
def newplan():
    user_id = session['user_id']

    if request.method == 'POST':

        topic_name = request.form['topicName']
        subtopic_names = request.form.getlist('subtopic')
        subtopic_hours = request.form.getlist('subtopicHours')

        conn = get_db()
        with conn:
            topic_id = add_topic(conn, (topic_name, user_id))
            for name, hours in zip(subtopic_names, subtopic_hours):
                subtopics = (topic_id, name, hours, user_id)
                add_subtopic(conn, subtopics)

        session['has_plan'] = True

        return redirect(url_for('userlog.userlog'))
    
    else:

        conn = get_db()
        with conn:
            # If user already has a plan, can't make another one
            if user_has_plan(conn, (user_id, )):
                return render_template('ooops.html', 
                                        error_code='Hi there,',
                                        message='it seems like you already have a plan.', 
                                        btn_href='/userlog',
                                        btn_title='Go to your log')

        return render_template('newplan.html')
