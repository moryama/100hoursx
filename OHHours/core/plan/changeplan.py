import functools
from flask import Blueprint, redirect, render_template, request, session, url_for

from OHHours.helpers import login_required  
from OHHours.models.db import get_db
from OHHours.models.add import add_subtopic
from OHHours.models.retr import retrieve_subtopics, retrieve_topic, user_has_plan
from OHHours.models.upd import update_subtopic
from OHHours.models.remv import delete_subtopic

bp = Blueprint('changeplan', __name__,
               template_folder='templates')

@bp.route('/changeplan', methods=['GET', 'POST'])
@login_required
def change_plan():
    '''Apply changes to learning plan'''

    user_id = session['user_id']

    if request.method == 'POST':

        # Prepare data for usage
        subtopic_names = request.form.getlist('subtopic-name')
        goal_hours = request.form.getlist('subtopic-hours')
        subtopic_ids = request.form.getlist('subtopic-id')
        topic_id = request.form.get('topic-id')
        to_delete = request.form.getlist('to-delete')

        subtopics = [] # a list of tuples, each tuple a subtopic

        for name, hours, id in zip(subtopic_names, goal_hours, subtopic_ids):
            subtopics.append((name, hours, id))

        conn = get_db()
        with conn:
            for subtopic in subtopics:
                if subtopic[2].startswith('new'):
                    add_subtopic(conn, (topic_id, name, hours, user_id))
                else:
                    update_subtopic(conn, subtopic)

            # Delete subtopic in the 'to_delete' list, if any:
            if to_delete:
                for subtopic_id in to_delete:
                    delete_subtopic(conn, (subtopic_id, ))

        return redirect(url_for('userlog.userlog'))
    
    else:

        # Retrieve learning plan from database
        conn = get_db()
        with conn:

            if not user_has_plan(conn, (user_id, )):
                return redirect(url_for('userlog.userlog'))

            topic_id, topic_name = retrieve_topic(conn, (user_id, ))
            subtopics_data = retrieve_subtopics(conn, (topic_id,))

        subtopics, subtopic = [], {}
        total_hours = 0
        for row in subtopics_data:
            subtopic_id, subtopic_name, goal_hours, done_minutes = row
            subtopic['id'], subtopic['name'], subtopic['hours'] = subtopic_id, subtopic_name, goal_hours            
            total_hours += goal_hours
            subtopics.append(subtopic.copy()) # without copy(), only the reference to the dictionary will be appended

        return render_template('changeplan.html', 
                               topic=topic_name, 
                               topic_id=topic_id, 
                               subtopics=subtopics, 
                               total_hours=total_hours)

