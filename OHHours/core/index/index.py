import functools
from flask import (
    Blueprint, redirect, render_template, request, session
)
import datetime
import calendar

bp = Blueprint('index', __name__, 
                     template_folder='templates')

@bp.route('/')
def index():
    """Welcome page"""
    
    today = datetime.date.today()

    day = today.day
    month = today.strftime('%B')

    future = (today + datetime.timedelta(6 * 365 / 12))
    future_month= future.strftime('%B')

    return render_template("index.html", day=day, month=month, future_month=future_month)


