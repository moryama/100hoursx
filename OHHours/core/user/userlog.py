import functools
import datetime

from flask import Blueprint, redirect, render_template, request, session

from OHHours.helpers import login_required 
from OHHours.models.add import add_session
from OHHours.models.upd import update_minutes
from OHHours.models.retr import retrieve_last_session_datetime, retrieve_resources, retrieve_subtopics, retrieve_topic, user_has_plan
from OHHours.models.db import get_db
from OHHours.helpers import round_hours

bp = Blueprint('userlog', __name__, 
                        template_folder='templates')


@bp.route('/userlog', methods=['GET', 'POST'])
@login_required
def userlog():

    user_id = session['user_id']

    if request.method == 'POST':
        # Collect and add session data to database
        current_date = datetime.datetime.today()
        subtopic_id = request.form['session-subtopic']
        curr_session_minutes = request.form['session-minutes']
    
        conn = get_db()
        with conn:
            
            session_data = (subtopic_id, current_date, curr_session_minutes, user_id)
            session_id = add_session(conn, session_data)

            # Get current done minutes for the subtopic
            db = conn.cursor()
            db.execute('SELECT doneMinutes FROM subtopics WHERE subtopicId=?',
                              (subtopic_id, ))
            row = db.fetchall()
            # Sum the new value to the done minutes
            total_minutes = row[0][0] + int(curr_session_minutes)
            subtopic_data = (total_minutes, subtopic_id)
            update_minutes(conn, subtopic_data)

        return redirect('/userlog')

    else:    
        
        conn = get_db()

        # Check when last session was recorded
        with conn:
            last_session_datetime = retrieve_last_session_datetime(conn, (user_id, ))
            last_session_date = last_session_datetime[0].split()[0]

        with conn:

            # Return empty log template if no plan exists yet
            if not user_has_plan(conn, (user_id, )):
                return render_template('emptylog.html')

            topic_id, topic_name = retrieve_topic(conn, (user_id, ))
            subtopics_data = retrieve_subtopics(conn, (topic_id, ))
        
            subtopics, subtopic = [], {}
            total_done_minutes = 0  
            total_goal_hours = 0
            subtopics_ids = []
            for row in subtopics_data:
                subtopic_id, subtopic_name, goal_hours, done_minutes = row

                # Calculate time data
                total_done_minutes += done_minutes # for the whole topic
                to_do_minutes = (goal_hours * 60) - done_minutes # for each subtopic
                total_goal_hours += goal_hours
                # Convert the minutes into hours and round up
                total_done_hours = round_hours(total_done_minutes) # for the whole topic
                done_hours = round_hours(done_minutes) # for each subtopic
                to_do_hours = round_hours(to_do_minutes) # for each subtopic
                # Calculate percentages to show on progress bar
                subtopic_percentage_done = round((done_minutes / (goal_hours * 60)) * 100)
                subtopic_percentage_todo = round(100 - subtopic_percentage_done)

                # Pack all data to display on page
                subtopic['id'], subtopic['name'], subtopic['donehours'], subtopic['todohours'], subtopic['percentage_done'], subtopic['percentage_todo'] = subtopic_id, subtopic_name, done_hours, to_do_hours, subtopic_percentage_done, subtopic_percentage_todo

                # Collect subtopic ids for later use
                subtopics_ids.append(subtopic_id)

                subtopics.append(subtopic.copy())

            # Also retrieve resources from database
            resources = {}
            conn = get_db()
            with conn:
                for subtopic_id in subtopics_ids:
                    rows = retrieve_resources(conn, (subtopic_id, ))
                    try:
                        resources[subtopic_id] = rows[0][0]
                    except:
                        resources[subtopic_id] = 'You can add useful resources here.'

        return render_template('userlog.html', 
                                resources=resources, 
                                topic=topic_name, 
                                topic_id=topic_id, 
                                total_done_hours=total_done_hours, 
                                subtopics=subtopics, 
                                total_goal_hours=total_goal_hours, 
                                last_session_date=last_session_date)
    