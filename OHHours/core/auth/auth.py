import functools

from flask import (Blueprint, g, redirect, render_template, request, session, url_for)
from werkzeug.security import check_password_hash, generate_password_hash

from OHHours.models.db import get_db
from OHHours.models.auth import register_user, retrieve_user, user_has_plan, username_available

bp = Blueprint('auth', __name__,
                    template_folder='templates')


@bp.route('/register', methods=['GET', 'POST'])
def register():
    '''Register user'''

    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        pw_confirmation = request.form['confirmation']

        # Check user input is valid
        if not username:
            return render_template('ooops.html', 
                                    error_code=400,
                                    message='Please choose a username.', 
                                    btn_href='/register',
                                    btn_title='Back to registration')
        elif not password or not pw_confirmation:
            return render_template('ooops.html', 
                                    error_code=400,
                                    message='Please choose a password.', 
                                    btn_href='/register',
                                    btn_title='Back to registration')
        elif password != pw_confirmation:
            return render_template('ooops.html', 
                                    error_code=400,
                                    message='Your password and confirmation don\'t match', 
                                    btn_href='/register',
                                    btn_title='Back to registration')

        # If username is not taken, add new user to database
        conn = get_db()
        with conn:   
            if not username_available(conn, (username,)):
                return render_template('ooops.html', 
                                       error_code=400,
                                       message='This username is taken, could you think of a different one?', 
                                       btn_href='/register',
                                       btn_title='Back to registration')

            user_id = register_user(conn, (username, generate_password_hash(password)))

        # Log user in
        session['user_id'] = user_id
        session['has_plan'] = False

        return redirect(url_for('newplan.newplan'))
        
    else:
        
        try:
            if session['user_id']:
                return redirect(url_for('userlog.userlog'))
        except KeyError:
            return render_template('register.html')



@bp.route('/login', methods=['GET', 'POST'])
def login():
    '''Log user in'''

    # Forget any user_id
    session.clear()

    if request.method == 'POST':

        username = request.form['username']
        password = request.form['password']

        # Ensure username and password were submitted
        if not username:
            return render_template('ooops.html', 
                                    error_code=403,
                                    message='Please provide a username.', 
                                    btn_href='/login',
                                    btn_title='Back to login')


        elif not password:
            return render_template('ooops.html', 
                                    error_code=403,
                                    message='Please provide a password.', 
                                    btn_href='/login',
                                    btn_title='Back to login')

        # Ensure username exists and password is correct
        conn = get_db()
        with conn:
            user = retrieve_user(conn, (username, ))

        if user is None: 
            return render_template('ooops.html', 
                                    error_code=403,
                                    message='Please provide a valid username.', 
                                    btn_href='/login',
                                    btn_title='Back to login')
        else:
            user_id, username, pw_hash = user
        
        if not check_password_hash(pw_hash, password):
            return render_template('ooops.html', 
                                    error_code=403,
                                    message='Please provide a valid password.', 
                                    btn_href='/login',
                                    btn_title='Back to login')

        # Remember user
        session['user_id'] = user_id
        session['username'] = username

        # Check if user has a plan in database
        conn = get_db()
        with conn:
            if user_has_plan(conn, (user_id, )):
                session['has_plan'] = True
            else:
                session['has_plan'] = False

        return redirect(url_for('userlog.userlog'))
        
    else:
        return render_template('login.html')


@bp.route('/logout')
def logout():
    '''Log user out'''

    # Forget any user
    session.clear()

    # Redirect user to login form
    return redirect('/')




    